#include <cstddef>
#include <stdio.h>      
#include <stdlib.h> 


#define MAXINT 100000


int* stackCreate(std::size_t size)
{
    int n = size%sizeof(int*);
    
    int* array = static_cast<int *> (std::malloc(sizeof(int*)*n));

    //std::cout << "stack created" << std::endl;

    return array;

}

template <typename TypeT>
void stackDelete(TypeT array)
{

    std::free(array);
    //std::cout << "stack deleted" << std::endl;
}




class ramStack
{
    int** blockList = nullptr;
    int* bottom = nullptr;   //pointer to stack_bottom
    int* top = nullptr;     //pointer to the top element (not array)
    size_t currentblock = 0; //shows which block of stack memory we are in
    size_t height = 0; //independently of of block size shows how many elements (Type*) are in stack
    size_t block;
private:
    size_t blocksize; //how many elements fit in one block
    size_t blocklenght; //amount of TypeT pointers per block
    void appendBlock(){
        ///TO DO somehow implement a next pointer in the stack class
        this->currentblock++;
        this->blockList[currentblock] = stackCreate(blocksize);
    }
    void removeBlock(){
        std::free(this->blockList[currentblock]);
        this->blockList[currentblock] == nullptr;
        this->currentblock--;
        
    }
public:
    ramStack(size_t n){
        this->blockList = static_cast<int** >(std::malloc( sizeof(int*) * MAXINT ));
        this->blocksize = n;
        this->bottom = stackCreate(this->blocksize);


        //this->blockList[0][0]=this->bottom; // compile error, type covnersion int -> int*


        this->top = &this->blockList[0][0];
        this->blocklenght = this->blocksize/sizeof(int*);


        //this->bottom->next=NULL;
    }
    void push(int t){
        this->height++;
        if(this->height%this->blocklenght == 0){ 
            this->appendBlock();
            this->top == &blockList[currentblock][0]; //avoiding this one modulo operation
            return;
        }   
        this->top == &blockList[currentblock][this->height%blocklenght];
    }
    void pop(){


        //this->blockList[currentblock][this->height%this->blocklenght]=nullptr; // compile error, type covnersion int -> int*


        if(this->height%this->blocklenght == 0){ 
            this->removeBlock();
            this->height--;
            this->blockList[currentblock][blocksize-1]; //avoiding this one modulo operation
            return;
        }  
        this->height--;
        this->top == &blockList[currentblock][this->height%blocklenght];
    }
    int* top_(){ // variable name and function name collision
        return this->top;
    }
    bool empty(){
        return this->height == 0&&this->currentblock==0;
    }
};


